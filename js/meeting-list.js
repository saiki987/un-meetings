// https://www.un.org/press/en/content/general-assembly/meetings-coverage
meetings = []
document.querySelectorAll('.view-content .views-row').forEach((article) => {
    const meeting = {}
    meeting['date'] = new Date(
        article.querySelector('.date-display-single').getAttribute('content')
    ).toISOString().slice(0,10)
    meeting['title'] = article.querySelector('.views-field-title a').innerText
    meeting['detail_url'] = article.querySelector('.views-field-title a').href
    meetings.push(meeting)
})
JSON.stringify(meetings, null, 4)
meetings.map(m => Object.values(m).map(m2 => JSON.stringify(m2)).join(',')).join('\r\n')
return meetings
