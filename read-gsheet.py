from dotenv import load_dotenv
import os
import pickle
from googleapiclient.discovery import build

load_dotenv('.env')
SPREADSHEET_ID = os.environ.get('UN_MEETINGS_SHEET_ID')
RANGE_NAME = 'A1:B10'

if __name__ == "__main__":
    # craete-token-file.pyにより作成
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SPREADSHEET_ID, range=RANGE_NAME).execute()
    values = result.get('values', [])
    print(values)

